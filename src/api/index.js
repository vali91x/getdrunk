import axios from 'axios'

const FOURSQUARE_API_PATH = process.env.FOURSQUARE_API_PATH
const FOURSQUARE_API_VERSION = process.env.FOURSQUARE_API_VERSION

const credentials = {
  client_id: process.env.CLIENT_ID,
  client_secret: process.env.CLIENT_SECRET
}

export default {
  getVenues: async (parameters, onSuccess, onError) => {
    let location = {}
    location[parameters.locationType] = parameters.location
    try {
      let response = await axios({
        method: 'GET',
        url: `${FOURSQUARE_API_PATH}/venues/explore`,
        params: {
          ...credentials,
          ...location,
          v: FOURSQUARE_API_VERSION,
          radius: parameters.radius,
          query: parameters.query,
          price: parameters.price.join(',')
        }
      })
      return onSuccess(response.data.response)
    } catch (error) {
      return onError(error)
    }
  }
}
