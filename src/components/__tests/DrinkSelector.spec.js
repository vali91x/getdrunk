import { shallowMount, createLocalVue } from '@vue/test-utils'
import DrinkSelector from '../../components/DrinkSelector.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('DrinkSelector', () => {
  let getters = {}
  let store = new Vuex.Store({ getters })
  let cmp = shallowMount(DrinkSelector, { store, localVue })

  it('has the expected html structure', () => {
    expect(cmp.element).toMatchSnapshot()
  })
})
