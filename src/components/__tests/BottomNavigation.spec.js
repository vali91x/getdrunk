import { shallowMount, createLocalVue } from '@vue/test-utils'
import BottomNavigation from '../../components/BottomNavigation.vue'
import Vuex from 'vuex'

const localVue = createLocalVue()
localVue.use(Vuex)

describe('BottomNavigation', () => {
  let cmp
  let getters
  let store

  beforeEach(() => {
    getters = {
      'ui/getStep': () => 1,
      'ui/canGoForward': () => true
    }
    store = new Vuex.Store({
      getters
    })

    cmp = shallowMount(BottomNavigation, { store, localVue })
  })

  it('has the expected html structure', () => {
    expect(cmp.element).toMatchSnapshot()
  })

  it('goForward method commits in the ui module', () => {
    cmp.vm.$store.commit = jest.fn()
    cmp.vm.goForward()
    expect(cmp.vm.$store.commit).toHaveBeenCalledWith('ui/GO_FORWARD')
  })

  it('goBackwards method commits in the ui module', () => {
    cmp.vm.$store.commit = jest.fn()
    cmp.vm.goBackwards()
    expect(cmp.vm.$store.commit).toHaveBeenCalledWith('ui/GO_BACKWARDS')
  })
})
