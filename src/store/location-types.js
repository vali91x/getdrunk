const locationTypes = {
  LL: 'll', // Latitude and longitude of the user’s location.
  NEAR: 'near' // A string naming a place in the world
}

export default locationTypes
