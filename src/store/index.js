import Vue from 'vue'
import Vuex from 'vuex'
import ui from './modules/ui'
import venues from './modules/venues'

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    ui,
    venues
  }
})
