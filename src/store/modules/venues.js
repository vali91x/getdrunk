import api from '../../api'
import * as types from '../mutation-types'
import locationTypes from '../location-types'

const namespaced = true

const state = {
  location: {
    type: locationTypes.LL,
    ll: null,
    near: null
  },
  radius: 5000,
  type: 'Beer Bar',
  price: [2, 3],
  venues: []
}
const getters = {
  getVenues: state => state.venues,
  getVenuesCount: state => state.venues.length,
  getLocationType: state => state.location.type,
  getLocation: state => state.location.type === locationTypes.LL ? state.location.ll : state.location.near,
  getRadius: state => state.radius,
  getType: state => state.type,
  getPrice: state => state.price
}

const actions = {
  async getVenues ({ commit, state }) {
    await api.getVenues(
      {
        radius: state.radius,
        query: state.type,
        price: state.price,
        locationType: state.location.type,
        location: state.location.type === locationTypes.LL ? state.location.ll : state.location.near
      },
      (response) => {
        commit(types.GET_VENUES, response.groups[0].items)
      },
      (error) => {
        console.log('Error while retrieving the venues', error)
      }
    )
  }
}

const mutations = {
  [types.GET_VENUES] (state, venues) {
    state.venues = venues.map(obj => {
      let parsedVenue = {}
      parsedVenue.name = obj.venue.name
      const location = obj.venue.location
      parsedVenue.address = `${location.address}, ${location.city}, ${location.country}`
      return parsedVenue
    })
  },
  [types.SET_LOCATION_COORDINATES] (state, { latitude, longitude }) {
    state.location.type = locationTypes.LL
    state.location.ll = `${latitude},${longitude}`
  },
  [types.SET_LOCATION_STRING] (state, value) {
    state.location.type = locationTypes.NEAR
    state.location.near = value
  },
  [types.SET_RADIUS] (state, radius) {
    state.radius = radius
  },
  [types.SET_TYPE] (state, type) {
    state.type = type
  },
  [types.SET_PRICE] (state, price) {
    state.price = price
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
