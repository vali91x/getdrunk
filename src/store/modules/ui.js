import * as types from '../mutation-types'

const namespaced = true

const state = {
  step: 0,
  allowNextStep: true
}
const getters = {
  getStep: state => state.step,
  canGoForward: state => state.allowNextStep
}

const actions = {}

const mutations = {
  [types.GO_FORWARD] (state) {
    state.step += 1
  },
  [types.GO_BACKWARDS] (state) {
    state.step -= 1
  },
  [types.ALLOW_NEXT_STEP] (state, value) {
    state.allowNextStep = value
  }
}

export default {
  namespaced,
  state,
  getters,
  actions,
  mutations
}
