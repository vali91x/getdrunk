

# getDrunk app!

> Code Challenge using FourSquare API


Working demo: [https://getdrunkapp-9ea76.firebaseapp.com/](https://getdrunkapp-9ea76.firebaseapp.com/)

## Instructions

#### Install dependencies

    npm install
    yarn


#### Create .env file. Add `client_id` & `client_secret`


    module.exports = {
       CLIENT_ID: '"{client_id}"',
       CLIENT_SECRET: '"{client_secret}"',
       FOURSQUARE_API_PATH: '"https://api.foursquare.com/v2"',
       FOURSQUARE_API_VERSION: '"20180802"'
    };


#### Serve with hot reload at localhost:8080

    npm run dev
    yarn dev


#### Build for production with minification

    npm run build
    yarn build


#### Build for production and view the bundle analyzer report

    npm run build --report
    yarn build --report


#### Run unit tests

    npm run unit
    yarn unit


#### Run all tests

    npm test
    yarn test
