'use strict'
const env = require('../.env')

module.exports = {
  NODE_ENV: '"production"',
  CLIENT_ID: env.CLIENT_ID,
  CLIENT_SECRET: env.CLIENT_SECRET,
  FOURSQUARE_API_PATH: env.FOURSQUARE_API_PATH,
  FOURSQUARE_API_VERSION: env.FOURSQUARE_API_VERSION
}
